import React, { useState } from 'react';
import { Input, Layout, Typography } from 'antd';
import { Table } from 'antd';
import axios from 'axios'

import './App.css'

import 'antd/dist/antd.css';

const { Content } = Layout;
const { Title } = Typography;

const columns = [
  {
    title: 'avatar_url',
    dataIndex: 'avatar_url',
    key: 'avatar_url',
    sorter: (a, b) => a.avatar_url.length - b.avatar_url.length,
  },
  {
    title: 'login',
    dataIndex: 'login',
    key: 'login',
    sorter: (a, b) => a.login.length - b.login.length,
  },
  {
    title: 'type',
    dataIndex: 'type',
    key: 'type',
  },
];

const App = () => {
  const [search, setSearch] = useState('')
  const [result, setResult] = useState([])
  const [total, setTotal] = useState(0)
  const [page, setPage] = useState(1)

  const onChange = (e) => {
    setSearch(e.target.value);

    if (e.target.value) {
    axios.get(`https://api.github.com/search/users?q=${e.target.value}&per_page=9`)
      .then((res) => {
        setResult(res.data.items?.map(item =>({ avatar_url: item.avatar_url, login: item.login, type: item.type, key: item.avatar_url })))
        setTotal(res.data.total_count)
        setPage(1)
      });
    }
  }

  const onPaginationChange = (page) => {
    axios.get(`https://api.github.com/search/users?q=${search}&per_page=9&page=${page}`)
    .then((res) => {
      setResult(res.data.items?.map(item =>({ avatar_url: item.avatar_url, login: item.login, type: item.type, key: item.avatar_url })))
      setPage(page)
    });
  }

  return (
    <Layout>
        <Content>
          <div className="wrapper">
            <Title level={2}>Piao test task</Title>
            <Input
              value={search}
              placeholder="Your query"
              onChange={onChange}
              id="query-search"
            />
            <Table pagination={{ total, pageSize: 9, onChange: onPaginationChange, current: page }} dataSource={result} columns={columns} />
          </div>
        </Content>
    </Layout>
  );
}

export default App;
